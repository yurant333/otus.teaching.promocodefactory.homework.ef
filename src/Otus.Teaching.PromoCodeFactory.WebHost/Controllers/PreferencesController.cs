﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Предпочтения
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferencesController
    : ControllerBase
{
    private IRepository<Preference> _preferencesRepository;
    
    public PreferencesController(IRepository<Preference> preferenceRepository)
    {
        _preferencesRepository = preferenceRepository;
    }
    
    /// <summary>
    /// Получение списка предпочтений
    /// </summary>
    /// <param name="model">Предпочтения</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<PreferenceResponse>> GetCustomersAsync()
    {
        var preferences = await _preferencesRepository.GetAllAsync();
        var preferencesList = preferences.Select(p=> 
            new PreferenceResponse()
            {
                Id = p.Id,
                Name = p.Name,
            }).ToList();
        return preferencesList;
    }
}