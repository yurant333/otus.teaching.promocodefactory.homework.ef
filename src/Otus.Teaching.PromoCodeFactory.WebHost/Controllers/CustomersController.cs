﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _customersRepository;
        private IRepository<Preference> _preferencesRepository;
        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customersRepository = customerRepository;
            _preferencesRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <param name="model">Клиент</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();
            var customersShortList = customers.Select(c=> 
                new CustomerShortResponse()
                {
                    Id = c.Id,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Email = c.Email
                }).ToList();
            return customersShortList;
        }
        
        /// <summary>
        /// Получение клиента по id
        /// </summary>
        /// <param name="model">Клиент</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            
            var customerResponse = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences?.Select(p =>
                    new PreferenceShortResponse()
                    {
                        Name = p.Name
                    }).ToList() ?? new List<PreferenceShortResponse>(),

                PromoCode = customer.PromoCode != null ? new PromoCodeShortResponse()
                {
                    Id = customer.PromoCode.Id,
                    Code = customer.PromoCode.Code,
                    ServiceInfo = customer.PromoCode.ServiceInfo,
                    BeginDate = customer.PromoCode.BeginDate.ToString(),
                    EndDate = customer.PromoCode.EndDate.ToString(),
                    PartnerName = customer.PromoCode.PartnerName
                } : null
            };
            
            return customerResponse;
        }
        
        /// <summary>
        /// Добавление клиента
        /// </summary>
        /// <param name="model">Клиент</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync([FromBody]CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
    
            var preferenceIds = request.PreferenceIds.ToArray(); // Convert to an array
    
            var preferences = await _preferencesRepository.GetAllAsync();
            
            preferences = preferences.Where(p => preferenceIds.Contains(p.Id)).ToList(); // Execute the query
    
            foreach (var preference in preferences)
            {
                preference.Customers.Add(customer);
            }
    
            await _customersRepository.CreateAsync(customer);
            
            foreach (var preference in preferences)
            {
                await _preferencesRepository.UpdateAsync(preference);
            }

            return Ok(customer.Id);
        }

        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <param name="model">Клиент</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, [FromBody]CreateOrEditCustomerRequest request)
        {
            var currentCustomer = await _customersRepository.GetByIdAsync(id);

            if (currentCustomer == null) return NotFound("Customer is not exist!");
            else
            {
                currentCustomer.FirstName = request.FirstName;
                currentCustomer.LastName = request.LastName;
                currentCustomer.Email = request.Email;
                await _customersRepository.UpdateAsync(currentCustomer); 
            }

            if (request.PreferenceIds.Any())
            {
                var preferences = await _preferencesRepository.GetAllAsync();
                
                foreach (var preference in preferences)
                {
                    preference.Customers.Remove(currentCustomer);
                }
                
                await _preferencesRepository.UpdateRangeAsync(preferences);
                
                var newPreferencesList = preferences.Where(p=>request.PreferenceIds.Contains(p.Id));
                
                foreach (var preference in newPreferencesList)
                {
                    preference.Customers.Add(currentCustomer);
                    await _preferencesRepository.UpdateAsync(preference);
                }
                
                await _preferencesRepository.UpdateRangeAsync(newPreferencesList);
            }

            return Ok();
        }
        
        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="model">Клиент</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteCustomer(Guid id)
        {
            var custumer = await _customersRepository.GetByIdAsync(id);

            if (custumer == null) return BadRequest("Customer is not found!");

            var result = await _customersRepository.DeleteAsync(custumer);
            
            if (result == true)
                return Ok();
            else
                return BadRequest();
        }
    }
}