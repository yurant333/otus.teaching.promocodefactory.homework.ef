﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        Task<T> CreateAsync(T tEntity);
        Task<T> UpdateAsync(T tEntity);
        Task<bool> DeleteAsync(T tEntity);

        Task<IEnumerable<T>> UpdateRangeAsync(IEnumerable<T> tEntities);
    }
}