﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Required]
        [MaxLength(20)]
        public string Code { get; set; }

        [Required]
        [MaxLength(50)]
        public string ServiceInfo { get; set; }
        
        [Required]
        public DateTime BeginDate { get; set; }
        
        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        [MaxLength(50)]
        public string PartnerName { get; set; }

        public virtual Employee PartnerManager { get; set; }

        public virtual List<Preference> Preferences { get; set; }
        
        public virtual Customer Customer { get; set; }
    }
}