﻿#nullable enable
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {   
        public Customer()
        {
            Preferences = new List<Preference>();
        }
        
        [MaxLength(30)]
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }
        
        public string FullName => $"{FirstName} {LastName}";
        
        [Required]
        [MaxLength(40)]
        public string Email { get; set; }

        public virtual List<Preference> Preferences { get; set; }
        
        public virtual PromoCode? PromoCode { get; set; }
    }
}