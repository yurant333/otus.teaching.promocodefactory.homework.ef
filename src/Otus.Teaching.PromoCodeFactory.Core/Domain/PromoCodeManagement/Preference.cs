﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public Preference()
        {
            Customers = new List<Customer>();
        }
        
        [MaxLength(30)]
        [Required]
        public string Name { get; set; }
        
        public virtual List<Customer> Customers { get; set; }
    }
}