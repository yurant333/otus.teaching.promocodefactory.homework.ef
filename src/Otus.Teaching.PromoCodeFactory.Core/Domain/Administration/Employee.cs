﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(20)]
        [Required]
        public string FirstName { get; set; }
        
        [MaxLength(20)]
        [Required]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [MaxLength(30)]
        public string Email { get; set; }

        [Required]
        public int AppliedPromocodesCount { get; set; }
        
        public virtual List<PromoCode> PromoCodes { get; set; }
        
        public virtual List<Role> Roles { get; set; }
    }
}