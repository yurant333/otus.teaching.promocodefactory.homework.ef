﻿using System;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Options;

public static class Extension
{
    public static IServiceCollection ConfigureDbServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
        
        services.AddDbContext<PromoCodeFactoryContext>(
            options =>
            {
                var migrationsAssemblyName = typeof(PromoCodeFactoryContext).GetTypeInfo().Assembly.GetName().Name;
                var connectionString = configuration.GetConnectionString("PromoCodeFactoryDb");
                options.UseSqlite(connectionString, x => x.MigrationsAssembly(migrationsAssemblyName));
                options.UseLazyLoadingProxies();
            });

        services.AddScoped<IDbInitializer, DbInitialiser>();

        return services;
    }
    
    public static void Migration(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var databaseContext = scope.ServiceProvider.GetRequiredService<PromoCodeFactoryContext>();
        
        if (databaseContext.Database.CanConnect())
        {
            databaseContext.Database.EnsureDeleted();
        }
        
        if (!databaseContext.Database.CanConnect())
        {
            databaseContext.Database.Migrate();
        }
    }
    public static void DataSeed(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var databaseContext = scope.ServiceProvider.GetRequiredService<PromoCodeFactoryContext>();
        if (databaseContext.Database.CanConnect())
        {
            DbInitialiser.Initialize(databaseContext);
        }
    }
    
}
