﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Options
{
    public class DbInitialiser : IDbInitializer
    {
        public static void Initialize(PromoCodeFactoryContext context)
        {
            /*context.Employee.RemoveRange(context.Employee);
            context.Role.RemoveRange(context.Role);
            context.Preference.RemoveRange(context.Preference);
            context.Customer.RemoveRange(context.Customer);
            context.PromoCode.RemoveRange(context.PromoCode);
            context.SaveChanges();*/

            context.Employee.AddRange(FakeDataFactory.Employees);
            context.Customer.AddRange(FakeDataFactory.Customers);
            context.SaveChanges();
        }
    }
}