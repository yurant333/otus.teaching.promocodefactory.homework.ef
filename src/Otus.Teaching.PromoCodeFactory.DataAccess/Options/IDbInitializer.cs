﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Options;

public interface IDbInitializer
{
    public void Initialize()
    {}
}