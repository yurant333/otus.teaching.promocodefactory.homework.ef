﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T>
    : IRepository<T>
    where T: BaseEntity
{
    private DbContext _dbContext { get; set; }
    
    public EfRepository(PromoCodeFactoryContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<IEnumerable<T>> GetAllAsync()
    {
        var resultList = _dbContext.Set<T>().ToList();
        return await Task.FromResult(resultList);
    }

    public async Task<T> GetByIdAsync(Guid id)
    {
        var resultT = await _dbContext.Set<T>().FirstOrDefaultAsync(r => r.Id == id);
        return resultT;
    }

    public async Task<T> CreateAsync(T entity)
    {
        _dbContext.Set<T>().Entry(entity).State = EntityState.Added;
        await _dbContext.SaveChangesAsync();
        return entity;
    }

    public async Task<T> UpdateAsync(T entity)
    {
        _dbContext.Set<T>().Entry(entity).State = EntityState.Modified;
        await _dbContext.SaveChangesAsync();
        return entity;
    }
    
    public async Task<IEnumerable<T>> UpdateRangeAsync(IEnumerable<T> entities)
    {
        foreach (var entity in entities)
        {
            _dbContext.Set<T>().Entry(entity).State = EntityState.Modified;
        }
        await _dbContext.SaveChangesAsync();
        return entities;
    }
    
    public async Task<bool> DeleteAsync(T entity)
    {
        var flagDelete = false;
        
        _dbContext.Set<T>().Entry(entity).State = EntityState.Deleted;
        await _dbContext.SaveChangesAsync();

        flagDelete = true;
        return flagDelete;
    }
}