﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context;

public class PromoCodeFactoryContextFactory : IDesignTimeDbContextFactory<PromoCodeFactoryContext>
{
    public PromoCodeFactoryContext CreateDbContext(string[] args)
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../Otus.Teaching.PromoCodeFactory.WebHost"))
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables();

        var configuration = builder.Build();
        
        var connectionString = configuration.GetConnectionString( "PromoCodeFactoryDb");

        var migrationsAssemblyName = typeof(PromoCodeFactoryContext).GetTypeInfo().Assembly.GetName().Name;

        var optionsBuilder = new DbContextOptionsBuilder<PromoCodeFactoryContext>()
            .UseSqlite(connectionString, sqlServerOptions => sqlServerOptions.MigrationsAssembly(migrationsAssemblyName));

        return new PromoCodeFactoryContext(optionsBuilder.Options);
    }
}