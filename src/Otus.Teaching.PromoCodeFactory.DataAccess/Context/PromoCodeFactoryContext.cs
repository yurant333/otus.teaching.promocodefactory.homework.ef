﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context;

public class PromoCodeFactoryContext : DbContext 
{
    public PromoCodeFactoryContext (DbContextOptions<PromoCodeFactoryContext> options)
        : base(options)
    {
    }

    public DbSet<Employee> Employee { get; set; }
    public DbSet<Role> Role { get; set; }
    public DbSet<Customer> Customer { get; set; }
    public DbSet<Preference> Preference { get; set; }
    public DbSet<PromoCode> PromoCode { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Employee>().
            HasMany(e=>e.PromoCodes).
            WithOne(p=>p.PartnerManager).
            HasForeignKey(p=>p.Id);

        modelBuilder.Entity<Employee>()
            .HasMany(e=>e.Roles)
            .WithMany(r=>r.Employees)
            .UsingEntity(er => er.ToTable("EmployeesRoles"));

        modelBuilder.Entity<Customer>()
            .HasOne(p => p.PromoCode)
            .WithOne(c => c.Customer)
            .HasForeignKey<PromoCode>(c => c.Id)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Customer>()
            .HasMany(c => c.Preferences)
            .WithMany(p => p.Customers)
            .UsingEntity(cp => cp.ToTable("CustomerPreference"));
    }
}